/*!
Extended probabilistic computation tree logic
*/
use super::*;

/// A PCTL* state formula over a set of atomic propositions
#[derive(Debug, Clone, Eq)]
pub enum StateFormula<A, R = f64> {
    /// The formula which is always true
    True,
    /// The formula which is always false. Technically `not(true)`, but we use this to avoid allocations.
    False,
    /// An atomic proposition
    Atom(A),
    /// The conjunction of two state formulas
    And(aops::And<Arc<Self>>),
    /// The negation of a state formula
    Not(aops::Not<Arc<Self>>),
    /// With probability ~p, a given path formula is satisfied
    Pr(aops::Pr<Arc<PathFormula<A, R>>, R>),
    /// A wrapped state formula
    Id(Arc<Self>),
}

impl<A: PartialEq, R: PartialEq> PartialEq for StateFormula<A, R> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Id(this), Id(other)) => this.unboxed().eq(other.unboxed()),
            (Id(this), other) => this.unboxed().eq(other),
            (this, Id(other)) => this.eq(other.unboxed()),
            (True, True) => true,
            (False, False) => true,
            (Atom(a), Atom(b)) => a.eq(b),
            (And(this), And(other)) => this.eq(other),
            (Not(this), Not(other)) => this.eq(other),
            (Pr(this), Pr(other)) => this.eq(other),
            _ => false,
        }
    }
}

impl<A: Hash, R: Hash> Hash for StateFormula<A, R> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let this = self.unboxed();
        std::mem::discriminant(this).hash(state);
        match this {
            Atom(atom) => atom.hash(state),
            And(conj) => conj.hash(state),
            Not(phi) => phi.hash(state),
            Pr(psi) => psi.hash(state),
            _ => {}
        }
    }
}

impl<A, R> StateFormula<A, R> {
    /// Convert this formula to an `Arc`
    pub fn into_arc(self) -> Arc<Self> {
        match self {
            Id(arc) => arc,
            phi => Arc::new(phi),
        }
    }
    /// unboxed this formula
    pub fn unboxed(&self) -> &Self {
        match self {
            Id(arc) => arc.unboxed(),
            phi => phi,
        }
    }
}

use StateFormula::*;

impl<A, R> From<bool> for StateFormula<A, R> {
    fn from(b: bool) -> Self {
        if b {
            True
        } else {
            False
        }
    }
}

impl<A, R> ops::BitAnd for StateFormula<A, R> {
    type Output = Self;
    fn bitand(self, other: StateFormula<A, R>) -> StateFormula<A, R> {
        match (self, other) {
            (True, other) => other,
            (False, _) => False,
            (this, True) => this,
            (_, False) => False,
            (this, other) => And(aops::And::new(this.into_arc(), other.into_arc())),
        }
    }
}

impl<A, R> ops::BitOr for StateFormula<A, R> {
    type Output = Self;
    fn bitor(self, other: Self) -> Self {
        !(!self & !other)
    }
}

impl<A, R> ops::Shr for StateFormula<A, R> {
    type Output = Self;
    fn shr(self, other: Self) -> Self {
        !self | other
    }
}

impl<A, R> ops::Shl for StateFormula<A, R> {
    type Output = Self;
    fn shl(self, other: Self) -> Self {
        self | !other
    }
}

impl<A, R> ops::Not for StateFormula<A, R> {
    type Output = Self;
    fn not(self) -> StateFormula<A, R> {
        match self {
            True => False,
            False => True,
            Not(phi) => Id(phi.into_phi()),
            Pr(psi) => Pr(!psi),
            phi => Not(aops::Not::new(phi.into_arc())),
        }
    }
}

/// A PCTL* path formula over a set of atomic proposititions
#[derive(Debug, Clone, Eq, Hash)]
pub enum PathFormula<A, R = f64> {
    /// A state formula
    State(StateFormula<A, R>),
    /// A conjunction of path formulas
    And(aops::And<Arc<PathFormula<A, R>>>),
    /// The negation of a path formula
    Not(aops::Not<Arc<PathFormula<A, R>>>),
    /// The next state satisfies the given formula
    Next(aops::Next<StateFormula<A, R>>),
    /// All states in the path satisfy the first formula until the second formula is satisfied
    Until(aops::Until<StateFormula<A, R>>),
    /// A wrapped path formula
    Id(Arc<PathFormula<A, R>>),
}

impl<A: PartialEq, R: PartialEq> PartialEq for PathFormula<A, R> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (PId(this), PId(other)) => this.unboxed().eq(other.unboxed()),
            (PId(this), other) => this.unboxed().eq(other),
            (this, PId(other)) => this.eq(other.unboxed()),
            (State(this), State(other)) => this.eq(other),
            (PAnd(this), PAnd(other)) => this.eq(other),
            (PNot(this), PNot(other)) => this.eq(other),
            (Next(this), Next(other)) => this.eq(other),
            (Until(this), Until(other)) => this.eq(other),
            _ => false,
        }
    }
}

impl<A, R> PathFormula<A, R> {
    /// Convert this formula to an `Arc`
    pub fn into_arc(self) -> Arc<Self> {
        match self {
            PId(arc) => arc,
            phi => Arc::new(phi),
        }
    }
    /// unboxed this formula
    pub fn unboxed(&self) -> &Self {
        match self {
            PId(arc) => arc.unboxed(),
            phi => phi,
        }
    }
}

use PathFormula::{And as PAnd, Id as PId, Next, Not as PNot, State, Until};

impl<A, R> From<bool> for PathFormula<A, R> {
    fn from(b: bool) -> Self {
        if b {
            State(True)
        } else {
            State(False)
        }
    }
}

impl<A, R> ops::BitAnd for PathFormula<A, R> {
    type Output = Self;
    fn bitand(self, other: Self) -> Self {
        match (self, other) {
            (State(True), other) => other,
            (State(False), _) => State(False),
            (this, State(True)) => this,
            (_, State(False)) => State(False),
            (State(this), State(other)) => State(this & other),
            (this, other) => PAnd(aops::And::new(this.into_arc(), other.into_arc())),
        }
    }
}

impl<A, R> ops::BitOr for PathFormula<A, R> {
    type Output = Self;
    fn bitor(self, other: Self) -> Self {
        !(!self & !other)
    }
}

impl<A, R> ops::Shr for PathFormula<A, R> {
    type Output = Self;
    fn shr(self, other: Self) -> Self {
        !self | other
    }
}

impl<A, R> ops::Shl for PathFormula<A, R> {
    type Output = Self;
    fn shl(self, other: Self) -> Self {
        self | !other
    }
}

impl<A, R> ops::Not for PathFormula<A, R> {
    type Output = Self;
    fn not(self) -> Self {
        match self {
            State(state) => State(!state),
            psi => PNot(aops::Not::new(psi.into_arc())),
        }
    }
}

//TODO: derived operators (e.g. F, G...)

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn bool_pctl_star_operations() {
        let a = || Atom::<_>(());
        for &l in &[true, false] {
            let ls = StateFormula::from(l);
            for &r in &[true, false] {
                let rs = StateFormula::from(r);
                assert_eq!(ls.clone() & rs.clone(), StateFormula::from(l & r));
                assert_eq!(ls.clone() | rs.clone(), StateFormula::from(l | r));
                assert_eq!(ls.clone() >> rs.clone(), StateFormula::from(!l | r));
                assert_eq!(ls.clone() << rs, StateFormula::from(l | !r));
            }
            assert_eq!(!ls.clone(), StateFormula::from(!l));
            assert_eq!(ls & a(), if l { a() } else { False });
        }
    }
}
