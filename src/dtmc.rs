/*!
Discrete-time Markov chains
*/

use super::*;
use fxhash::FxHashSet;
use sprs::CsMat;

/// Compute the set of states which satisfy a PCTL formula in the DTMC with a given start and labelling function
pub fn pctl_sat<P, L, A, R>(
    transition_probabilities: &CsMat<P>,
    labeling: &mut L,
    psi: &pctl::StateFormula<A, R>,
) -> FxHashSet<usize>
where
    L: FnMut(&A, usize) -> bool,
{
    //TODO: allocate capacity
    let mut result = FxHashSet::default();
    pctl_sat_edit(transition_probabilities, labeling, psi, &mut result, true);
    result
}

/// Add or remove to a set the states which satisfy a PCTL formula in the DTMC with a given start and labelling function
pub fn pctl_sat_edit<P, L, A, R>(
    _transition_probabilities: &CsMat<P>,
    _labeling: &mut L,
    _psi: &pctl::StateFormula<A, R>,
    _set: &mut FxHashSet<usize>,
    _add: bool,
) where
    L: FnMut(&A, usize) -> bool,
{
    todo!()
}
