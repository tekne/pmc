/*!
Computation tree logic
*/
use super::*;

/// A CTL state formula over a set of atomic propositions
#[derive(Debug, Clone, Eq)]
pub enum StateFormula<A> {
    /// The formula which is always true
    True,
    /// The formula which is always false. Technically `not(true)`, but we use this to avoid allocations.
    False,
    /// An atomic proposition
    Atom(A),
    /// The conjunction of two state formulas
    And(aops::And<Arc<Self>>),
    /// The negation of a state formula
    Not(aops::Not<Arc<Self>>),
    /// For all paths, the path formula is satisfied
    Forall(aops::Forall<Arc<PathFormula<A>>>),
    /// There exists a path such that the path formula is satisfied
    Exists(aops::Exists<Arc<PathFormula<A>>>),
    /// A wrapped state formula
    Id(Arc<Self>),
}

impl<A: PartialEq> PartialEq for StateFormula<A> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Id(this), Id(other)) => this.unboxed().eq(other.unboxed()),
            (Id(this), other) => this.unboxed().eq(other),
            (this, Id(other)) => this.eq(other.unboxed()),
            (True, True) => true,
            (False, False) => true,
            (Atom(a), Atom(b)) => a.eq(b),
            (And(this), And(other)) => this.eq(other),
            (Not(this), Not(other)) => this.eq(other),
            (Forall(phi), Forall(psi)) => phi.eq(psi),
            (Exists(phi), Exists(psi)) => phi.eq(psi),
            _ => false,
        }
    }
}

impl<A: Hash> Hash for StateFormula<A> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let this = self.unboxed();
        std::mem::discriminant(this).hash(state);
        match this {
            Atom(atom) => atom.hash(state),
            And(conj) => conj.hash(state),
            Not(phi) => phi.hash(state),
            Forall(psi) => psi.hash(state),
            Exists(psi) => psi.hash(state),
            _ => {}
        }
    }
}

impl<A> StateFormula<A> {
    /// Convert this formula to an `Arc`
    pub fn into_arc(self) -> Arc<Self> {
        match self {
            Id(arc) => arc,
            phi => Arc::new(phi),
        }
    }
    /// unboxed this formula
    pub fn unboxed(&self) -> &Self {
        match self {
            Id(arc) => arc.unboxed(),
            phi => phi,
        }
    }
}

use StateFormula::*;

impl<A> From<bool> for StateFormula<A> {
    fn from(b: bool) -> Self {
        if b {
            True
        } else {
            False
        }
    }
}

impl<A> ops::BitAnd for StateFormula<A> {
    type Output = Self;
    fn bitand(self, other: Self) -> Self {
        match (self, other) {
            (True, other) => other,
            (False, _) => False,
            (this, True) => this,
            (_, False) => False,
            (this, other) => And(aops::And::new(this.into_arc(), other.into_arc())),
        }
    }
}

impl<A> ops::BitOr for StateFormula<A> {
    type Output = Self;
    fn bitor(self, other: Self) -> Self {
        !(!self & !other)
    }
}

impl<A> ops::Shr for StateFormula<A> {
    type Output = Self;
    fn shr(self, other: Self) -> Self {
        !self | other
    }
}

impl<A> ops::Shl for StateFormula<A> {
    type Output = Self;
    fn shl(self, other: Self) -> Self {
        self | !other
    }
}

impl<A> ops::Not for StateFormula<A> {
    type Output = Self;
    fn not(self) -> Self {
        match self {
            True => False,
            False => True,
            Not(phi) => Id(phi.into_phi()),
            phi => Not(aops::Not::new(phi.into_arc())),
        }
    }
}

/// A CTL path formula over a set of atomic proposititions
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum PathFormula<A> {
    /// The next state satisfies the given formula
    Next(aops::Next<StateFormula<A>>),
    /// All states in the path satisfy the given formula
    Global(aops::Global<StateFormula<A>>),
    /// All states in the path satisfy the first formula until the second formula is satisfied, which must be satisfied in at most k steps
    BoundedUntil(aops::BoundedUntil<StateFormula<A>>),
    /// All states in the path satisfy the first formula until the second formula is satisfied
    Until(aops::Until<StateFormula<A>>),
}

//TODO: derived operators (e.g. F)

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn bool_ctl_operations() {
        let a = || Atom(());
        for &l in &[true, false] {
            let ls = StateFormula::from(l);
            for &r in &[true, false] {
                let rs = StateFormula::from(r);
                assert_eq!(ls.clone() & rs.clone(), StateFormula::from(l & r));
                assert_eq!(ls.clone() | rs.clone(), StateFormula::from(l | r));
                assert_eq!(ls.clone() >> rs.clone(), StateFormula::from(!l | r));
                assert_eq!(ls.clone() << rs, StateFormula::from(l | !r));
            }
            assert_eq!(!ls.clone(), StateFormula::from(!l));
            assert_eq!(ls & a(), if l { a() } else { False });
        }
    }
}
