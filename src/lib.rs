/*!
Probabilistic model checking tools
*/

pub mod ctl;
pub mod dtmc;
pub mod ltl;
pub mod lts;
pub mod pctl;
pub mod pctl_star;

use elysees::Arc;
use num::BigUint;
use std::hash::{Hash, Hasher};
use std::ops;

/// A relationship between two values
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Rel {
    Lt,
    Gt,
    Le,
    Ge,
}

impl ops::Not for Rel {
    type Output = Self;
    fn not(self) -> Rel {
        match self {
            Rel::Lt => Rel::Ge,
            Rel::Gt => Rel::Le,
            Rel::Le => Rel::Gt,
            Rel::Ge => Rel::Lt,
        }
    }
}

/// Abstract syntax tree operations
pub mod aops {
    use super::*;

    /// Negation
    #[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
    pub struct Not<P> {
        phi: P,
    }

    impl<P> Not<P> {
        #[inline]
        pub fn new(phi: P) -> Not<P> {
            Not { phi }
        }
        #[inline]
        pub fn phi(&self) -> &P {
            &self.phi
        }
        #[inline]
        pub fn into_phi(self) -> P {
            self.phi
        }
    }

    /// Conjunction
    #[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
    pub struct And<P> {
        left: P,
        right: P,
    }

    impl<P> And<P> {
        #[inline]
        pub fn new(left: P, right: P) -> And<P> {
            And { left, right }
        }
        #[inline]
        pub fn from_inner((left, right): (P, P)) -> And<P> {
            And { left, right }
        }
        #[inline]
        pub fn left(&self) -> &P {
            &self.left
        }
        #[inline]
        pub fn right(&self) -> &P {
            &self.right
        }
        #[inline]
        pub fn into_inner(self) -> (P, P) {
            (self.left, self.right)
        }
    }

    /// Forall operator
    #[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
    pub struct Forall<P> {
        psi: P,
    }

    impl<P> Forall<P> {
        #[inline]
        pub fn new(psi: P) -> Forall<P> {
            Forall { psi }
        }
        #[inline]
        pub fn inner(&self) -> &P {
            &self.psi
        }
        #[inline]
        pub fn into_psi(self) -> P {
            self.psi
        }
    }

    /// There exists operator
    #[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
    pub struct Exists<P> {
        psi: P,
    }

    impl<P> Exists<P> {
        #[inline]
        pub fn new(psi: P) -> Exists<P> {
            Exists { psi }
        }
        #[inline]
        pub fn psi(&self) -> &P {
            &self.psi
        }
        #[inline]
        pub fn into_psi(self) -> P {
            self.psi
        }
    }

    /// Probability bound operator
    #[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
    pub struct Pr<P, R = f64> {
        rel: Rel,
        p: R,
        psi: P,
    }

    impl<P, R> ops::Not for Pr<P, R> {
        type Output = Self;
        fn not(self) -> Self {
            Pr {
                rel: !self.rel,
                p: self.p,
                psi: self.psi,
            }
        }
    }

    impl<P, R: Copy> Pr<P, R> {
        #[inline]
        pub fn new(rel: Rel, p: R, psi: P) -> Self {
            Pr { rel, p, psi }
        }
        #[inline]
        pub fn from_inner((rel, p, psi): (Rel, R, P)) -> Self {
            Pr { rel, p, psi }
        }
        #[inline]
        pub fn rel(&self) -> Rel {
            self.rel
        }
        #[inline]
        pub fn p(&self) -> R {
            self.p
        }
        #[inline]
        pub fn psi(&self) -> &P {
            &self.psi
        }
        #[inline]
        pub fn into_psi(self) -> P {
            self.psi
        }
        #[inline]
        pub fn into_inner(self) -> (Rel, R, P) {
            (self.rel, self.p, self.psi)
        }
    }

    /// Next modality
    #[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
    pub struct Next<P> {
        psi: P,
    }

    impl<P> Next<P> {
        #[inline]
        pub fn new(psi: P) -> Next<P> {
            Next { psi }
        }
        #[inline]
        pub fn inner(&self) -> &P {
            &self.psi
        }
        #[inline]
        pub fn into_psi(self) -> P {
            self.psi
        }
    }

    /// Global modality
    #[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
    pub struct Global<P> {
        psi: P,
    }

    impl<P> Global<P> {
        #[inline]
        pub fn new(psi: P) -> Global<P> {
            Global { psi }
        }
        #[inline]
        pub fn inner(&self) -> &P {
            &self.psi
        }
        #[inline]
        pub fn into_psi(self) -> P {
            self.psi
        }
    }

    /// Until modality
    #[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
    pub struct Until<P> {
        first: P,
        until: P,
    }

    impl<P> Until<P> {
        #[inline]
        pub fn new(first: P, until: P) -> Until<P> {
            Until { first, until }
        }
        #[inline]
        pub fn first(&self) -> &P {
            &self.first
        }
        #[inline]
        pub fn until(&self) -> &P {
            &self.until
        }
        #[inline]
        pub fn into_inner(self) -> (P, P) {
            (self.first, self.until)
        }
    }

    /// Bounded until modality    
    #[derive(Debug, Clone, PartialEq, Eq, Hash)]
    pub struct BoundedUntil<P> {
        first: P,
        bound: BigUint,
        until: P,
    }

    impl<P> BoundedUntil<P> {
        #[inline]
        pub fn new(first: P, bound: BigUint, until: P) -> BoundedUntil<P> {
            BoundedUntil {
                first,
                bound,
                until,
            }
        }
        #[inline]
        pub fn first(&self) -> &P {
            &self.first
        }
        #[inline]
        pub fn bound(&self) -> &BigUint {
            &self.bound
        }
        #[inline]
        pub fn until(&self) -> &P {
            &self.until
        }
        #[inline]
        pub fn into_inner(self) -> (P, BigUint, P) {
            (self.first, self.bound, self.until)
        }
    }
}
