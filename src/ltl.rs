/*!
Linear time logic
*/
use super::*;

/// An LTL formula over a set of atomic propositions
#[derive(Debug, Clone, Eq)]
pub enum Formula<A> {
    /// The formula which is always true
    True,
    /// The formula which is always false. Technically `not(true)`, but we use this to avoid allocations.
    False,
    /// An atomic proposition
    Atom(A),
    /// The conjunction of two formulas
    And(aops::And<Arc<Self>>),
    /// The negation of a formula
    Not(aops::Not<Arc<Self>>),
    /// The next modality
    Next(aops::Next<Arc<Self>>),
    /// A formula is true until another one is
    Until(aops::Until<Arc<Self>>),
    /// A wrapped formula
    Id(Arc<Self>),
}

impl<A: PartialEq> PartialEq for Formula<A> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Id(this), Id(other)) => this.unboxed().eq(other.unboxed()),
            (Id(this), other) => this.unboxed().eq(other),
            (this, Id(other)) => this.eq(other.unboxed()),
            (True, True) => true,
            (False, False) => true,
            (Atom(a), Atom(b)) => a.eq(b),
            (And(this), And(other)) => this.eq(other),
            (Not(this), Not(other)) => this.eq(other),
            (Next(this), Next(other)) => this.eq(other),
            (Until(this), Until(other)) => this.eq(other),
            _ => false,
        }
    }
}

impl<A: Hash> Hash for Formula<A> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let this = self.unboxed();
        std::mem::discriminant(this).hash(state);
        match this {
            Atom(atom) => atom.hash(state),
            And(conj) => conj.hash(state),
            Not(phi) => phi.hash(state),
            _ => {}
        }
    }
}

impl<A> Formula<A> {
    /// Convert this formula to an `Arc`
    pub fn into_arc(self) -> Arc<Self> {
        match self {
            Id(arc) => arc,
            phi => Arc::new(phi),
        }
    }
    /// unboxed this formula
    pub fn unboxed(&self) -> &Self {
        match self {
            Id(arc) => arc.unboxed(),
            phi => phi,
        }
    }
}

use Formula::*;

impl<A> From<bool> for Formula<A> {
    fn from(b: bool) -> Self {
        if b {
            True
        } else {
            False
        }
    }
}

impl<A> ops::BitAnd for Formula<A> {
    type Output = Self;
    fn bitand(self, other: Self) -> Self {
        match (self, other) {
            (True, other) => other,
            (False, _) => False,
            (this, True) => this,
            (_, False) => False,
            (this, other) => And(aops::And::new(this.into_arc(), other.into_arc())),
        }
    }
}

impl<A> ops::BitOr for Formula<A> {
    type Output = Self;
    fn bitor(self, other: Self) -> Self {
        !(!self & !other)
    }
}

impl<A> ops::Shr for Formula<A> {
    type Output = Self;
    fn shr(self, other: Self) -> Self {
        !self | other
    }
}

impl<A> ops::Shl for Formula<A> {
    type Output = Self;
    fn shl(self, other: Self) -> Self {
        self | !other
    }
}

impl<A> ops::Not for Formula<A> {
    type Output = Self;
    fn not(self) -> Self {
        match self {
            True => False,
            False => True,
            Not(phi) => Id(phi.into_phi()),
            phi => Not(aops::Not::new(phi.into_arc())),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn bool_ltl_operations() {
        let a = || Atom(());
        for &l in &[true, false] {
            let ls = Formula::from(l);
            for &r in &[true, false] {
                let rs = Formula::from(r);
                assert_eq!(ls.clone() & rs.clone(), Formula::from(l & r));
                assert_eq!(ls.clone() | rs.clone(), Formula::from(l | r));
                assert_eq!(ls.clone() >> rs.clone(), Formula::from(!l | r));
                assert_eq!(ls.clone() << rs, Formula::from(l | !r));
            }
            assert_eq!(!ls.clone(), Formula::from(!l));
            assert_eq!(ls & a(), if l { a() } else { False });
        }
    }
}
